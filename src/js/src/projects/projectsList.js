"use strict";

var moduleDirective = function(prefixTransition, $timeout, STATE_NAMES) {
  var transform = prefixTransition.getPrefix('transform'),
    projects, directive;

  directive = {
    link: link,
    template: '<ul>\
                <li data-ng-repeat="item in projectsCtrl.projectsList">\
                  <project-thumb list="projectsCtrl.loadedThumbs"></project-thumb>\
                </li>\
              </ul>',
    restrict: 'E',
    controller: require('./projectsListController'),
    controllerAs: 'projectsListCtrl',
    replace: false
  }
  return directive;

  function link($scope, element, attrs, projectsListCtrl) {
    projects = projectsListCtrl.activeProjects;

    $timeout(function() {
      if (projectsListCtrl.deepLink.isTrue && projectsListCtrl.deepLink.stateName === STATE_NAMES.PROJECT) {
        angular.element(element.find('li')[projectsListCtrl.deepLink.id].querySelector('project-thumb')).triggerHandler('click');
      }
    });

    element.bind("mousemove", function(event) {
      for (var i = 0; i < projects.length; i++) {
        var project = projects[i],
          item, cx, cy, dx, dy, tiltx, tilty, radius, degree;

        item = project.getElementsByTagName("div")[0];
        cx = Math.ceil(project.offsetWidth / 2.0);
        cy = Math.ceil(project.offsetHeight / 2.0);
        dx = event.layerX - cx;
        dy = event.layerY - cy;
        tiltx = (dy / cy);
        tilty = -(dx / cx);
        radius = Math.sqrt(Math.pow(tiltx, 2) + Math.pow(tilty, 2));
        degree = (radius * 20);
        item.style[transform] = 'rotate3d(' + tiltx + ', ' + tilty + ', 0,' + degree + 'deg)';
      }
    });
  }
}

//moduleDirective.$inject = ['prefixTransition'];

module.exports = moduleDirective;
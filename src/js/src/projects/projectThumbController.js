"use strict";

module.exports = function($scope, projectsService, $rootScope) {
  var vm = this;
  vm.addThumb = addThumb;
  vm.loadedThumbsImage = projectsService.loadedThumbsImages;
  vm.removeThumb = removeThumb;
  vm.setCurrentThumb = setCurrentThumb;
  vm.openDetailsPage = openDetailsPage;
  vm.deepLink = projectsService.deepLink

  function addThumb(item) {
    projectsService.addThumb(item);
  }

  function removeThumb(item) {
    projectsService.removeThumb(item);
  }

  function setCurrentThumb(item) {
    projectsService.setCurrentThumb(item);
  }

  function openDetailsPage(settings) {
    projectsService.openedProjectSettings = settings;
  }
}
"use strict";

module.exports = function(prefixTransition, $timeout) {

  var transform = prefixTransition.getPrefix('transform');
  var directive = {
    restrict: 'E',
    require: ['^ngController'],
    controllerAs: 'projectDetailCtrl',
    compile: compile,
  }
  return directive;

  function compile(tElement, tAttrs) {
    tElement.append('<div id="gallery" class="black-bg">\
                  <div>\
                    <div data-ng-repeat="item in projectDetailCtrl.photoList track by $index" \
                         data-ng-show="item.id >= (projectDetailCtrl.currentImageIndex - 1) && item.id <= (projectDetailCtrl.currentImageIndex + 1)">\
                         <img data-ng-src="{{item.src}}" data-ng-class="{ \'img-archive\' : projectDetailCtrl.isArchive, \'img-project\' : !projectDetailCtrl.isArchive }">\
                    </div>\
                  </div>\
              </div>\
              <gallery-controls></gallery-controls>');
    return {
      post: postLink
    }
  }

  function postLink($scope, element, attrs) {

    $timeout(function() {
      var elements = element[0].querySelector("#gallery").getElementsByClassName('ng-scope');
      var list = [].slice.call(elements);

      $scope.$watch('currentImgH', function(newValue, oldValue) {
        if (newValue !== oldValue) {

          for (var i = 0; i < list.length; i++) {
            var style = {};
            style[transform] = 'rotateX(-' + 90 * i + 'deg) translate3d(0, 0, ' + Math.round(newValue / 2) + 'px)';
            angular.element(list[i]).css(style);
          }
        }
      });
    })
  }
}
"use strict";

module.exports = function($scope, projectsService, projectDetail, loadedImages, $rootScope, $state) {
  var vm = this;
  vm.settings = projectsService.openedProjectSettings;
  vm.projectThumbReset = projectThumbReset;
  vm.photoList = loadedImages;
  vm.projectDetail = projectDetail;
  vm.currentImageIndex = 0;
  vm.spinToImageIndex = 0
  vm.deepLink = projectsService.deepLink;
  vm.isArchive = $state.current.name.indexOf('archive') !== -1 || false;

  function projectThumbReset() {
    projectsService.resetThumb();
    vm.settings = null;
  }

  $scope.$watch(function() {
    return vm.spinToImageIndex;
  }, function(newValue, oldValue) {
    vm.spinToImageIndex = newValue;
    $scope.$broadcast('onImageChange');
  });
}
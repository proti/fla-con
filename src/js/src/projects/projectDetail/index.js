"use strict";

var projectDetail = angular.module('MainApp.projectDetail', ['ngSanitize']);

//directives
projectDetail.directive('projectDetailView', require('./projectDetailView'));
projectDetail.directive('projectGallery', require('./projectGallery'));
projectDetail.directive('galleryControls', require('./galleryControls'));

//controllers
projectDetail.controller('ProjectDetailController', require('./projectDetailController'));

module.exports = projectDetail;
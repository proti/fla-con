"use strict";

module.exports = function(prefixTransition, $timeout, $window) {
  var transitionEvent = prefixTransition.whichTransitionEvent(),
    transition = prefixTransition.getPrefix('transition'),
    transform = prefixTransition.getPrefix('transform');

  var directive = {
    link: link,
    replace: false,
    restrict: 'A'
  };
  return directive;

  function link($scope, element, attrs, projectDetailCtrl) {
    var controller = element.controller();
    var settings = controller.settings;
    var closeBt = angular.element(element.find('header')[0].children);
    var gallery = angular.element(element[0].querySelector("#gallery"));
    var spins = 0;
    var style = {};

    if (controller.deepLink.isTrue) {

    } else {
      element.css({
        'width': settings.offsetWidth + "px",
        'height': settings.offsetHeight + "px",
        'top': (settings.offsetTop - settings.scrollTop) + "px",
        'left': (settings.offsetLeft - settings.scrollLeft) + "px"
      });
      element.children().addClass('project-view-hide-content');
      element.removeClass('start');
      element.addClass('project-view-pre-transition');

      $timeout(function() {
        style[transform] = 'translate(' + settings.posX + 'px, ' + settings.posY + 'px) scale(' + settings.scaleX + ',' + settings.scaleY + ')';
        element.css(style);
        element.bind(transitionEvent, onProjectViewIn);
      }, 50);
    }

    function onProjectViewIn(event) {
      event.stopPropagation();
      element.unbind(transitionEvent, onProjectViewIn);
      element.attr('style', '');

      element.removeClass('project-view-pre-transition');
      element.addClass('project-view-transitioned-in');
      element.children().addClass('project-view-show-content transition-time');
      element.children().removeClass('project-view-hide-content');

      document.body.classList.add('no-scroll');

      closeBt.bind('click', onCloseBtClicked);

      /* responsive windowResize */
      $timeout(function() {
        angular.element($window).bind('resize', function() {
          $scope.currentImgH = (gallery.find('img')[controller.currentImageIndex]) ? gallery.find('img')[controller.currentImageIndex].offsetHeight : 0;
          style[transform] = 'translateZ(-' + Math.round($scope.currentImgH / 2) + 'px) rotateX(' + (controller.currentImageIndex * 90) + 'deg)'
          gallery.children().css(style)
          $scope.$apply();
        })
        angular.element($window).triggerHandler('resize');
      });

      /* end responsive windowResize */
    }

    function onCloseBtClicked(event) {
      element.children().bind(transitionEvent, onProjectViewHidden);
      element.children().removeClass('project-view-show-content');
      element.children().addClass('project-view-hide-content transition-time');
    }

    function onProjectViewHidden(event) {
      event.stopPropagation();
      element.children().unbind(transitionEvent, onProjectViewHidden);

      element.bind(transitionEvent, onProjectViewOut);

      var scrollTop = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
      var scaleX = settings.offsetWidth / document.body.clientWidth;
      var scaleY = settings.offsetHeight / document.body.clientHeight;
      var posX = settings.offsetLeft - (document.body.clientWidth / 2) + (settings.offsetWidth / 2);
      var posY = settings.offsetTop - (document.body.clientHeight / 2) + (settings.offsetHeight / 2) - scrollTop;

      style[transition] = 'transform 0.3s';
      style[transform] = 'translate(' + posX + 'px, ' + posY + 'px) scale(' + scaleX + ',' + scaleY + ')';
      element.css(style);
    }

    function onProjectViewOut(event) {
      event.stopPropagation();
      element.unbind(transitionEvent, onProjectViewOut);

      element.attr('style', '');
      element.addClass('start');
      element.removeClass('project-view-transitioned-in');

      controller.projectThumbReset();

      element.children().removeClass("transition-time");

      document.body.classList.remove('no-scroll');
    }
    //
    // +++++++++++++++++++++ ROTATE GALLERY CUBE ++++++++++++++++++++++++++++
    //
    $scope.$on('onImageChange', function() {
      spins = (controller.spinToImageIndex - controller.currentImageIndex);
      var dir = spins > 0 ? 1 : spins === 0 ? 0 : -1;
      if (dir !== 0) controller.currentImageIndex += dir;

      spinCube(controller.currentImageIndex);
    });

    function spinCube(dir) {
      style[transition] = 'transform ' + 1 / (Math.abs(spins) * 2) + 's ease-in-out';
      style[transform] = 'translateZ(-' + Math.round($scope.currentImgH / 2) + 'px) rotateX(' + (dir * 90) + 'deg)';
      gallery.children().css(style);

      if (spins !== 0) {
        gallery.bind(transitionEvent, onImageShown);
      }
    }

    function onImageShown(event) {
      event.stopPropagation();
      gallery.unbind(transitionEvent, onImageShown);
      spins = (controller.spinToImageIndex - controller.currentImageIndex);

      if (spins === 0) {
        style[transition] = 'none';
        gallery.children().css(style);
      } else {
        var dir = spins > 0 ? 1 : spins === 0 ? 0 : -1;
        if (dir !== 0) {
          controller.currentImageIndex += dir;
          $scope.$apply();
        }
        spinCube(controller.currentImageIndex);
      }
    }
  }
};
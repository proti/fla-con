"use strict";

module.exports = function() {
  var directive = {
    restrict: 'E',
    require: ['^ngController'],
    controllerAs: 'projectDetailCtrl',
    compile: compile
  }

  return directive;

  function compile(tElement, tAttrs) {

    tElement.append('<ul>\
                        <li data-ng-repeat="item in projectDetailCtrl.photoList track by $index"><input type="radio" id="radio-button-{{$index}}" name="select-img"/><label for="radio-button-{{$index}}" data-ng-click="onItemClick($index)"></label></li>\
                      </ul>');
    return {
      post: postLink
    }
  }

  function postLink($scope, element, attrs, projectDetailCtrl) {
    var projectDetailCtrl = projectDetailCtrl[0];

    $scope.onItemClick = function(index, event) {
      projectDetailCtrl.spinToImageIndex = index;
    }
  }
}
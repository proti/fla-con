"use strict";

module.exports = function($scope, projectsService) {
  var vm = this;
  vm.activeProjects = projectsService.thumbs;
  vm.deepLink = projectsService.deepLink;
}
"use strict";

var projects = angular.module('MainApp.projects', []);

//directives
projects.directive('projectThumb', require('./projectThumb'));
projects.directive('projectsList', require('./projectsList'));

//controllers
projects.controller('ProjectsController', require('./projectsController'));

//services
projects.factory('projectsService', require('./projectsService'));

module.exports = projects;
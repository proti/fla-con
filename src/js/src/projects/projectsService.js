"use strict";

module.exports = function($rootScope) {
  var service = {
    data: null,
    thumbs: [],
    loadedThumbsImages: [],
    currentThumb: null,
    addThumb: addThumb,
    setCurrentThumb: setCurrentThumb,
    removeThumb: removeThumb,
    resetThumb: resetThumb,
    resetCurrentThumb: false,
    openedProjectSettings: {},
    deepLink: {}
  };

  return service;

  function addThumb(item) {
    this.thumbs.push(item);
  }

  function removeThumb(item) {
    var i = this.thumbs.indexOf(item);
    this.thumbs.splice(i, 1);
  }

  function setCurrentThumb(item) {
    this.resetCurrentThumb = false;
    this.currentThumb = item;
    this.removeThumb(item);
  }

  function resetThumb() {
    this.resetCurrentThumb = true;
    this.removeThumb(this.currentThumb);
    this.openedProjectSettings = {};
    this.currentThumb = null;

    $rootScope.$broadcast('onProjectResetThumb');
  }
}
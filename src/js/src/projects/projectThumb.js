"use strict";

module.exports = function(prefixTransition, mouse, $state, $compile) {
  var transitionEvent = prefixTransition.whichTransitionEvent(),
    transition = prefixTransition.getPrefix('transition'),
    transform = prefixTransition.getPrefix('transform');

  var directive = {
    compile: compile,
    restrict: 'E',
    scope: {
      list: '='
    },
    replace: false,
    controller: require('./projectThumbController'),
    controllerAs: 'projectThumbCtrl',
    terminal: true,
    priority: 0
  };

  return directive;

  function compile(tElement, tAttr) {
    return {
      pre: preLink,
      post: postLink
    }
  }

  function preLink(scope, iElement, attrs, projectThumbCtrl) {
    var isArchive = iElement[0].parentNode.attributes.getNamedItem('data-ng-repeat').nodeValue.indexOf('archive') > -1 ? true : false;
    scope.isArchive = isArchive;
    var classPrefix = isArchive ? 'archive-thumb' : 'project-thumb';
    var bgClass = isArchive ? 'black-bg' : 'dark-bg';
    var img = scope.$parent.$index >= 0 ? scope.list[scope.$parent.$index].src : '';
    var bg = 'background: url(' + img + ') no-repeat; background-size: cover; background-position: center';

    iElement.append('<div>\
                      <div class="' + classPrefix + '-front blue-bg" style="' + bg + '"></div>\
                      <div class="' + classPrefix + '-back ' + bgClass + '"></div>\
                    </div>');

  }

  function postLink($scope, element, attrs, projectThumbCtrl) {
    var div = element.children();
    var li = element[0].parentNode;
    var style = {};
    var projectResetEvent;

    element.on('mouseover', onMouseOver);
    element.on('mouseout', onMouseOut);
    element.on('click', onClick);

    function onMouseOver(event) {
      if (!$scope.isClicked) {
        projectThumbCtrl.addThumb(li);
        style[transition] = 'transform 0.0s';
        div.css(style);
      }
    }

    function onMouseOut(event) {
      if (!$scope.isClicked) {
        projectThumbCtrl.removeThumb(li);
        style[transition] = 'transform 0.3s';
        style[transform] = 'none';
        div.css(style);
      }
    }

    function onClick(event) {
      if (!$scope.isClicked) {

        //find project-loader attribute in divs
        var loader = element[0].querySelector('[project-loader]');

        if (loader) {
          //re-compile back div to have project-loader working. As project-loader directive is not compiled during preLink phase of this directive
          $compile(loader)($scope);
        }

        var dir = (div[0].offsetWidth / 2 >= event.layerX) ? 1 : -1;
        $scope.isClicked = true;
        projectThumbCtrl.setCurrentThumb(li);
        style[transition] = 'transform 0.3s ease-in-out';
        style[transform] = 'rotate3d(0, ' + dir + ', 0, 180deg)';
        div.css(style);
        //when animation ends
        angular.element(this).bind(transitionEvent, onProjectThumbTransitionEnd);
        //listen for thumb reset from service???
        projectResetEvent = $scope.$on('onProjectResetThumb', onProjectReset);
      }
    };

    function onProjectThumbTransitionEnd(event) {
      angular.element(event.currentTarget).unbind(transitionEvent, onProjectThumbTransitionEnd);

      var scrollTop = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
      var scrollLeft = (document.documentElement && document.documentElement.scrollLeft) || document.body.scrollLeft;
      var posX = (document.body.clientWidth / 2) - div[0].offsetLeft - div[0].offsetWidth / 2;
      var posY = (document.body.clientHeight / 2) - div[0].offsetTop - div[0].offsetHeight / 2 + scrollTop;
      var scaleX = document.body.clientWidth / div[0].offsetWidth;
      var scaleY = document.body.clientHeight / div[0].offsetHeight;

      var settings = {
        posX: posX,
        posY: posY,
        scaleX: scaleX,
        scaleY: scaleY,
        offsetWidth: div[0].offsetWidth,
        offsetHeight: div[0].offsetHeight,
        offsetTop: div[0].offsetTop,
        offsetLeft: div[0].offsetLeft,
        scrollTop: scrollTop,
        scrollLeft: scrollLeft
      }

      projectThumbCtrl.openDetailsPage(settings);

      var stateName = $scope.isArchive ? 'home.archive' : 'home.project';
      var stateParams = {};

      if (projectThumbCtrl.deepLink.isTrue) {
        stateParams = {
          reload: stateName
        }
      }

      $state.go(stateName, {
        id: $scope.$parent.$index
      }, stateParams);
    }

    function onProjectReset(event, isClosed) { //need to pass event attribute to prevent FireFox gives an error "Event not defined"
      projectResetEvent(event); //$on returns a deregistration function for this listener. Calling it deregistrates the listener.

      var dir = (div.offsetWidth / 2 >= event.layerX) ? 1 : -1;
      var style = {};
      $scope.isClicked = false;
      style[transition] = 'transform 0.3s ease-in-out';
      style[transform] = 'rotate3d(0, ' + dir + ', 0, 0deg)';
      div.css(style);
      //back to home state
      $state.transitionTo('home');
    }
  }
};
"use strict";

module.exports = function($scope, data, loadedThumbs, projectsService) {

  projectsService.data = data;

  var vm = this;
  var projectsData = projectsService.data['projects'];

  vm.title = projectsData.title;
  vm.projectsList = projectsData.list;
  vm.loadedThumbs = loadedThumbs;
}
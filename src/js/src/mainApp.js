"use strict";

var dataResolver, projectResolver, mainApp, resolvedData;

var stateNames = {
  'ARCHIVE': 'archive',
  'PROJECT': 'project'
};

dataResolver = {
  data: function(loadData, projectsService) {
    if (!resolvedData) resolvedData = loadData.getDataFromJSON();
    return resolvedData;
  }
}

projectResolver = {
  projectDetail: function(stateName, loadData, projectsService, $stateParams) {
    dataResolver.data(loadData);
    return resolvedData.then(function(data) {
      projectsService.data = data;
      return data[stateName].list[$stateParams.id];
    });
  },
  loadImages: function(stateName, loadData, projectsService, $stateParams) {
    dataResolver.data(loadData);
    return resolvedData.then(function(data) {
      var pics = projectsService.data[stateName].list[$stateParams.id].photoList;
      return loadData.getImages(pics);
    });
  }
};

mainApp = angular.module('MainApp', [
  require('angular-ui-router'),
  require('./utils').name,
  require('./services').name,
  require('./header').name,
  require('./header/nav').name,
  require('./about').name,
  require('./projects').name,
  require('./projects/projectDetail').name,
  require('./archive').name,
  require('./contact').name
])
  .constant('STATE_NAMES', stateNames)
  .config(config)
  .run(run);

function config($stateProvider, $urlRouterProvider, $locationProvider, $anchorScrollProvider) {

  $anchorScrollProvider.disableAutoScrolling(); //prevent to scroll page to top after $state change.

  $urlRouterProvider.otherwise('/');
  $stateProvider
    .state('home', {
      url: '/',
      views: {
        'header': {
          templateUrl: 'header.html',
          controller: 'HeaderController',
          controllerAs: 'headerCtrl',
          resolve: dataResolver
        },
        'about': {
          templateUrl: 'about.html',
          controller: 'AboutController',
          controllerAs: 'aboutCtrl',
          resolve: dataResolver
        },
        'projects': {
          templateUrl: 'projects.html',
          controller: 'ProjectsController',
          controllerAs: 'projectsCtrl',
          resolve: {
            data: dataResolver.data,
            loadedThumbs: function(data, loadData) {
              var pics = [];
              data.projects.list.forEach(function(item) {
                pics.push(item.thumb);
              })
              return loadData.getImages(pics);
            }
          }
        },
        'archive-list': {
          templateUrl: 'archive-list.html',
          controller: 'ArchiveController',
          controllerAs: 'archiveCtrl',
          resolve: {
            data: dataResolver.data,
            loadedThumbs: function(data, loadData) {
              var pics = [];
              data.archive.list.forEach(function(item) {
                pics.push(item.thumb);
              })
              return loadData.getImages(pics);
            }
          }
        },
        'contact': {
          templateUrl: 'contact.html',
          controller: 'ContactController',
          controllerAs: 'contactCtrl',
          resolve: dataResolver
        },
        'project-details': {
          abstract: true,
          template: '<ui-view/>'
        }
      }
    })
    .state('home.project', {
      url: 'project/{id:[0-6]}',
      templateUrl: 'project-detail.html',
      controller: 'ProjectDetailController',
      controllerAs: 'projectDetailCtrl',
      resolve: {
        isDeepLink: function(projectsService) {
          return projectsService.deepLink.isTrue;
        },
        projectDetail: function(loadData, projectsService, $stateParams, isDeepLink) {
          if (!isDeepLink) return projectResolver.projectDetail('projects', loadData, projectsService, $stateParams);
        },
        loadedImages: function(loadData, projectsService, $stateParams, isDeepLink) {
          if (!isDeepLink) return projectResolver.loadImages('projects', loadData, projectsService, $stateParams);
        }
      }
    })
    .state('home.archive', {
      url: 'archive/{id:int}',
      templateUrl: 'project-detail.html',
      controller: 'ProjectDetailController',
      controllerAs: 'projectDetailCtrl',
      resolve: {
        isDeepLink: function(projectsService) {
          return projectsService.deepLink.isTrue;
        },
        projectDetail: function(loadData, projectsService, $stateParams, isDeepLink) {
          if (!isDeepLink) return projectResolver.projectDetail('archive', loadData, projectsService, $stateParams);
        },
        loadedImages: function(loadData, projectsService, $stateParams, isDeepLink) {
          if (!isDeepLink) return projectResolver.loadImages('archive', loadData, projectsService, $stateParams);
        }
      }
    });

  $locationProvider.html5Mode({
    enabled: true,
    rewriteLinks: true
  }); //removes # before the link url but need some .htaccess modifications as well
}

function run($rootScope, projectsService, $state) {

  projectsService.deepLink = {};

  $rootScope.$on('$stateChangeStart', function(e, to, toParams, from) {
    $rootScope.stateIsLoading = true; //show loader
    document.body.classList.add('no-scroll');
    if (from.name === "" && to.name !== "home") {
      projectsService.deepLink.isTrue = true;
      projectsService.deepLink.id = toParams.id;
      projectsService.deepLink.stateName = to.name.split(".")[1];
    } else {
      projectsService.deepLink.isTrue = false;
    }
  })

  $rootScope.$on('$stateChangeSuccess', function(event) {
    $rootScope.stateIsLoading = false; //hide loader
    document.body.classList.remove('no-scroll');
  });
}

module.exports = mainApp;
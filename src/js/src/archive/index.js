"use strict";

var archive = angular.module('MainApp.archive', []);

//directive
archive.directive('archiveList', require('./archiveList'));

//controller
archive.controller('ArchiveController', require('./archiveController'));
archive.controller('ArchiveListController', require('./archiveListController'));

module.exports = archive;
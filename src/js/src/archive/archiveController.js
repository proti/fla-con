"use strict";

module.exports = function($scope, data, loadedThumbs){
  var vm = this;
   vm.title = data['archive'].title;
   vm.archiveList = data['archive'].list;
   vm.loadedThumbs = loadedThumbs;
}
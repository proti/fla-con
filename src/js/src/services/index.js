"use strict";

var services = angular.module('MainApp.services', []);

services.service('loadData', require('./loadData'));

module.exports = services;
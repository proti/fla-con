"use strict";

module.exports = function($http, $q) {

  this.getDataFromJSON = function() {
    var defer = $q.defer();

    $http.get('data/data.json')
      .success(function(response) {
        defer.resolve(response);
      })
      .error(function(error, status) {
        defer.reject(error);
      });

    return defer.promise.then(function(data) {
      return data;
    });
  },

  this.getImages = function(imagesToLoad) {
    var images = [];
    var imagesToLoadLength = imagesToLoad.length;
    var imagesLoaded = 0;
    var defer = $q.defer();

    for (var i = 0; i < imagesToLoadLength; i++) {
      images[i] = new Image();
      images[i].id = i;
      images[i].onload = function(event) {
        imagesLoaded++;
        if (imagesLoaded === imagesToLoadLength) defer.resolve(images);
      };
      images[i].src = imagesToLoad[i];
    }

    return defer.promise.then(function(data) {
      return data;
    });
  }
}
"use strict";

module.exports = function() {
  var location = null;
  var previousLocation = null;
  var factory = {

    getLocation: function() {
      return (
        angular.copy(location)
      );
    },

    setLocation: function(x, y) {
      if (location) {
        previousLocation = location;
      }
      location = {
        x: x,
        y: y
      };
    }
  }

  return factory;
};
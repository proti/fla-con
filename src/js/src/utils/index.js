"use strict";

var utils = angular.module('MainApp.utils', []);

utils.factory('prefixTransition', require('./prefixTransition'));
utils.factory('mouse', require('./mouse'));

module.exports = utils;
"use strict";

module.exports = function() {
  var factory = {

    getPrefix: function(prop) {
      var i, s = document.createElement('p').style,
        v = ['ms', 'O', 'Moz', 'Webkit'];

      if (s[prop] === '') return prop;

      prop = prop.charAt(0).toUpperCase() + prop.slice(1);

      for (i = v.length; i--;) {
        if (s[v[i] + prop] === '') return (v[i] + prop);
      }
    },

    whichTransitionEvent: function() {
      var t, el = document.createElement("fakeelement");
      var transitions = {
        "transition": "transitionend",
        "OTransition": "oTransitionEnd",
        "MozTransition": "transitionend",
        "WebkitTransition": "webkitTransitionEnd",
        "msTransition": "msTransitionEnd"
      }
      for (t in transitions) {
        if (el.style[t] !== undefined) {
          return transitions[t];
        }
      }
    }
  }
  return factory;
};
"use strict";

module.exports = function() {
  var controller, scope;

  var directive = {
    restrict: 'A',
    replace: false,
    link: link
  }

  return directive;

  function link($scope, element, attrs) {
    controller = element.controller();
    scope = $scope;
    element.on("submit", submitForm);
  }

  function submitForm(evt) {
    //scope.input is an object containg all necessary data.
    controller.sendForm(scope.input).then(function(response) {
      if (response) scope.success = response;
      else scope.error = true;
    })
  }
}
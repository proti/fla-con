"use strict";

module.exports = function($http, $q) {

  this.sendContactForm = function(postData) {
    return $http.post('/contactSend.php', postData, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      })
      .then(function(response) {
          return response.data;
        },
        function(error) {
          return error.data;
        });
  }
}
"use strict";

module.exports = function($scope, data, sendFormService) {
  var vm = this,
    contact = data['contact'];

  vm.title = contact.title;
  vm.name = contact.name;
  vm.email = contact.email;
  vm.message = contact.message;
  vm.send = contact.send;
  vm.messageSent = contact.messageSent;
  vm.error = contact.error;
  vm.allfFieldsAreRequired = contact.allfFieldsAreRequired;
  vm.nameTooShort = contact.nameTooShort;
  vm.validEmail = contact.validEmail;
  vm.msgToShort = contact.msgToShort;

  vm.sendForm = function(postData) {
    return sendFormService.sendContactForm(postData).then(function(response) {
      return response === 'success' ? true : false;
    });
  }
}
"use strict";

var contact = angular.module('MainApp.contact', []);

contact.directive('contactForm', require('./contactForm'));

contact.controller('ContactController', require('./contactController'));

contact.service('sendFormService', require('./sendFormService'));

module.exports = contact;
"use strict";

module.exports = function($scope, data) {
  var vm = this;
  vm.title = data['about'].title;
  vm.descr = data['about'].descr;
}
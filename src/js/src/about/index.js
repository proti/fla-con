"use strict";

var about = angular.module('MainApp.about', []);

about.controller('AboutController', require('./aboutController'));

module.exports = about;
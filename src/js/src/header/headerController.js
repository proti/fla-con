"use strict";

module.exports = function($scope, data, projectsService) {
  var vm = this;
  vm.header = data['header'];
  vm.nav = data['nav'];
  vm.changeZIndex = false;
  vm.currentNavSelected = '';

  $scope.$watch(function() {
    return $scope.currentNavSelected;
  }, function(newValue, oldValue) {
    if (newValue !== oldValue) {
      vm.currentNavSelected = newValue;
    }
  });

  $scope.$watch(function() {
    return projectsService.openedProjectSettings;
  }, function(newValue, oldValue) {
    if (newValue !== oldValue) {
      vm.changeZIndex = !vm.changeZIndex;
    } else {
      vm.changeZIndex = vm.changeZIndex;
    }
  });
}
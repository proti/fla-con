"use strict";

var smoothScroll = require('../../utils/smoothScroll');

module.exports = function($timeout) {
  var directive = {
    link: link,
    restrict: 'E',
    template: '<ul>\
                <li ng-repeat="item in headerCtrl.nav">\
                  <a ui-sref="home({ \'#\': \'{{ item.sref }}\' })" data-ng-click="scrollTo($event, item.sref);" fla-touchstart="scrollTo($event, item.sref);"><span data-hover="{{ item.label }}" data-ng-class="{active : item.sref === headerCtrl.currentNavSelected}">{{ item.label }}</span></a>\
                </li>\
              </ul>'

  }
  return directive;

  function link($scope, $element, $attr, headerCtrl) {
    var navHeight = 0;

    $scope.scrollTo = function(event, parm) {
      $scope.currentNavSelected = parm;

      navHeight = $element.parent()[0].offsetParent.clientHeight;
      smoothScroll(parm, -navHeight);
    }
  }
}
"use strict";

module.exports = function($window, $document, $timeout) {
  var uiViews = [],
    navItems = [],
    sections, pageYOffset = 0,
    maxScroll = 0,
    changePoint = 0,
    headerNavHeight = 0;
  var directive = {
    restrict: 'A',
    link: link
  }
  return directive;

  function link($scope, $element, $attr, $ctrl) {
    //$scope is the scope of the element where directive is implemented
    var currNavItem;

    $timeout(function() {
      headerNavHeight = $element.parent().parent()[0].clientHeight;
      Object.keys($element.find('a')).forEach(function(k) {
        if (!isNaN(k)) {
          var aTag = angular.element($element.find('a')[k]);
          var sref = aTag.scope().item.sref;
          navItems[sref] = {
            'a': aTag,
            'span': aTag.find('span')
          };
        }
      })

      if (uiViews.length === 0) {

        sections = angular.element($document[0].body).find('section');

        Object.keys(sections).forEach(function(key) {
          var attributes = angular.element(sections[key])[0].attributes;
          //select sections containing ui-view attribute only
          if (attributes && attributes.getNamedItem('ui-view')) {
            var getItemByAttrId = attributes.getNamedItem('ui-view');

            if (uiViews.indexOf(getItemByAttrId) == -1) {
              uiViews[getItemByAttrId.nodeValue] = getItemByAttrId.ownerElement;
            }
          }
        });
      }

      angular.element($window).bind('scroll touchmove', function(evt) {
        pageYOffset = this.pageYOffset;
        changePoint = Math.round(pageYOffset + ($document[0].body.clientHeight + headerNavHeight) / 3);
        maxScroll = $document[0].body.scrollHeight - $document[0].body.clientHeight;
        Object.keys(uiViews).forEach(function(skey) {
          //When page is almost at the top
          if (pageYOffset < headerNavHeight) {
            if (currNavItem) {
              currNavItem.span.removeClass('active');
              currNavItem.a.scope().currentNavSelected = null;
            }
            navItems['about'].a.scope().currentNavSelected = 'about';
            navItems['about'].span.addClass('active');
          } else if (pageYOffset >= maxScroll) {
            //End of page has been reached
            if (currNavItem) {
              currNavItem.span.removeClass('active');
              currNavItem.a.scope().currentNavSelected = null;
            }
            navItems['contact'].a.scope().currentNavSelected = 'contact';
            navItems['contact'].span.addClass('active');
          } else {
            if (uiViews[skey].offsetTop <= changePoint && (uiViews[skey].offsetTop + uiViews[skey].offsetHeight) > changePoint) {
              Object.keys(navItems).forEach(function(nkey) {
                navItems[nkey].span.removeClass('active');
                navItems[nkey].a.scope().currentNavSelected = null;
                if (uiViews[skey].getAttribute('ui-view').indexOf(nkey) !== -1) {
                  navItems[nkey].a.scope().currentNavSelected = nkey;
                  navItems[nkey].span.addClass('active');
                  currNavItem = navItems[nkey];
                }
              })
            }
          }
        })
      });
      angular.element($window).triggerHandler('scroll');
    });
  }
}
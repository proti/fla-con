"use strict";

var nav = angular.module('MainApp.nav', []);

//directives
nav.directive('nav', require('./nav'));
nav.directive('flaTouchstart', require('./flaTouchstart'));
nav.directive('navPageScroll', require('./navPageScroll'));
//controllers

module.exports = nav;
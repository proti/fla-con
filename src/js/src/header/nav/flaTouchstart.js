"use strict";

module.exports = function() {
  var directive = {
    link: link,
    restrict: 'A'

  }
  return directive;

  function link($scope, $element, $attr) {
    $element.bind('touchstart', onTouchStart);

    function onTouchStart(event) {
      var method = $element.attr('fla-touchstart');
      $scope.$event = event;
      $scope.$apply(method);
    };
  }
}
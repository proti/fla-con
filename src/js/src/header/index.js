"use strict";

var header = angular.module('MainApp.header', []);

//directives

//controllers
header.controller('HeaderController', require('./headerController'));

module.exports = header;